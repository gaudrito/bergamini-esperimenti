#!/bin/bash

java="java"
CP="bin:data/alchemist-redist-4.0.0.jar"

function usage() {
    echo "usages:"
    echo "    --help:                               show alchemist help"
    echo "    --compile:                            compile files into \"bin\" directory"
    echo "    --progress <prog> <points>:           track progress of a running process"
    echo "    --plot <prog> [title] [aggr names]:   build resuming plots"
    echo "    --gui <prog>:                         run program in alchemist gui"
    echo "    <prog> <step> <endtime> [variables]:  run batch tests"
    exit 0
}

if [ "$1" == "" ]; then
    usage
fi

if [ "$1" == "--help" ]; then
    if [ "$2" != "" ]; then
        usage
    fi
    $java -cp "$CP" it.unibo.alchemist.Alchemist -h
    exit 0
fi

if [ "$1" == "--compile" ]; then
    if [ "$2" != "" ]; then
        usage
    fi
    if [ ! -d "bin" ]; then
        mkdir bin
    fi
    cp src/main/protelis/* bin/
    javac `find src/main/java/ -name "*.java"` -d bin/ -cp "data/alchemist-redist-4.0.0.jar"
    exit 0
fi

if [ "$1" == "--progress" ]; then
    if [ "$3" == "" -o "$4" != "" ]; then
        usage
    fi
    exp="$2"
    pts="$3"
    while [ "`echo data/raw/"$exp"*`" == "data/raw/$exp*" ]; do
        sleep 2
        continue
    done
    perc=0
    while [ $perc -lt 100 ]; do
        sleep 2
        num=`ls data/raw/"$exp"* | wc -l`
        np=`cat data/raw/"$exp"* | grep -v "^#" | wc -l`
        np=$[(100*np/num-100)/pts]
        fin=`tail -n 1 data/raw/"$exp"* | grep "^#" | wc -l`
        if [ $np -gt $perc ]; then
            echo "$np%, $fin/$num completed  (`date`)"
        fi
        perc=$np
    done
    exit 0
fi

if [ "$1" == "--plot" ]; then
    if [ "$2" == "" ]; then
        usage
    fi
    exp="$2"
    title="$3"
    aggr="$4"
    shift 4
    if [ "$aggr" != "" ]; then
        for b in `ls data/raw/"$exp"* | sed "s|_$aggr-[0-9]*\.[0-9]*|$|" | sort | uniq`; do
            left=`echo $b | tr '$' '\n' | head -n 1`
            right=`echo $b | tr '$' '\n' | tail -n 1`
            for file in "$left"_"$aggr"-*.*"$right"; do
                name="$1"
                shift 1
                if [ "$name" == "" ]; then
                    name=`echo $file | sed "s|.*_$aggr-\([0-9]*\.[0-9]*\).*|\1|"`
                fi
                cat "$file" | sed "s|[-a-zA-Z]*\[[a-zA-Z]*\]|$name-&|g" > "$file.tmp"
            done
            paste -d ' ' "$left"_"$aggr"-*.*"$right".tmp > "$left$right"
            rm "$left"_"$aggr"-*.*"$right".tmp
        done
        for file in `ls data/raw/"$exp"*_"$aggr"-*`; do
            mv "$file" data/raw/detail.`basename "$file"`
        done
    fi
    file=`ls data/raw/"$exp"* | head -n 1`
    type=`cat "$file" | head -n 7 | tail -n 1 | tr -cd '@[' | tr -s '@['`
    if [ "$type" == "[" ]; then
        type="time"
    elif [ "$type" == "@" ]; then
        type="space 50 1000"
    fi
    for ((k=0; k<1000; k++)); do
        if [ ! -e "data/$exp.v$k.txt" ]; then break; fi
        if [ "$file" -ot "data/$exp.v$k.txt" ]; then break; fi
    done
    file="data/$exp.v$k.txt"
    echo -n > "$file"
    if [ "$title" != "" ]; then
        echo "# $title" >> "$file"
        echo "restart;" >> "$file"
        echo "with(plots):" >> "$file"
    fi
    i=0
    for b in `ls data/raw/"$exp"* | sed 's|_random-[0-9]*\.[0-9]*|$|' | sort | uniq`; do
        left=`echo $b | tr '$' '\n' | head -n 1`
        right=`echo $b | tr '$' '\n' | tail -n 1`
        echo -en "\033[1;4mBATCH $i\033[0;m  \t"
        if [ "$left" == "$b" ]; then
            ./plot_builder.py $type "$left" >> "$file"
        else
            ./plot_builder.py $type "$left"_random-*.0"$right" >> "$file"
        fi
        echo >> "$file"
        i=$[i+1]
    done
    exit 0
fi

if [ "$1" == "--gui" ]; then
    if [ "$2" == "" -o "$3" != "" ]; then
        usage
    fi
    prog="$2"
    $java -cp "$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -g src/main/resources/effects.aes
    exit 0
fi

if [ "$3" == "" -o "${1:0:1}" == "-" ]; then
    usage
fi

me="$0"
prog="$1"
file="$1"
step="$2"
time="$3"

if [ ! -d "data/raw" ]; then
    mkdir data/raw
fi

echo -n > nohup.out
shift 3
if [ "$1" == "" -o "${1:0:1}" == "-" ]; then
    nohup time -p $java -cp "$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -e "data/raw/$file" -i "$step" -t "$time" "$@" 2>>nohup.out &
else
    nohup time -p $java -cp "$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -e "data/raw/$file" -i "$step" -t "$time" -b -var "$@" 2>>nohup.out &
fi

pts=$[time/step]
nohup "$me" --progress "$prog" "$pts" 2>> nohup.out &
