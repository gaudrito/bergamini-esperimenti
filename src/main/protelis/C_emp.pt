/**
 * NOTE (basandosi sulle note nel commit 236800e)
 * 
 * 1. Il senso di accumulazione è stato invertito rispetto all'implementazione
 *    precendente. Ora i valori si accumulano verso la sorgente senza che sia
 *    necessario utilizzare l'opposto del gradiente.
 *                                                                                                                                    
 * 2. << Sia il campo d che p assegnano una grado di bontà scarso nei pressi 
 *    del device, per cui il campo risultante penalizza due volte i device 
 *    in quest'area >>.
 *    
 *    Inoltre il fattore d vuole assegnare un valore di bontà in base a
 *    quanto un device è lontano dalla circonferenza, e quindi non ha 
 *    senso penalizzare quelli che sono vicini al centro. Invece di usare 
 *    una funzione di Gauss, implementata dalla funzione d1, si potrebbe 
 *    utilizzare un'arcotangente, implementata dalla funzione d2. 
 * 
 *    Un'ulteriore possibilità, è una funzione direttamente proporzionale
 *    alla distanza dalla circonferenza, implementata dalla funzione d3. 
 *                                                                                                                                   
 * 3. << ... il fattore p può essere semplicemente il valore assoluto                                                                  
 *    della differenza di potenziale ... Inoltre, nel calcolo del fattore p, 
 *    può capitare che il potenziale sia ancora uguale ad infinito e quindi 
 *    il fattore stesso potrebbe essere uguale ad infinito o NaN ... >>
 *    
 *    La distinzione proposta per il fattore p era un tentativo di 
 *    eliminare le difficoltà introdotte dalla presenza di un potenziale
 *    infinito. Tuttavia è possibile realizzare una funzione p indipendente
 *    dalla metrica semplicemente considerando quei device per cui il
 *    potenziale non è infinito. Per tutti gli altri il fattore p sarà
 *    uguale a 0 e quindi anche il grado di bontà finale. Del resto se il
 *    potenziale è uguale ad infinito il device potrebbe addirittura non
 *    essere collegato alla sorgente.    
 * 
 *    NOTA :: sembra che non sia sufficiente utilizzare come condizione
 *            nbr(potential) != Infinity. E necessario utilizzare il metodo 
 *            statico isInfinite() definito nella classe Double di Java.
 * 
 *    NOTA :: il controllo sul potenziale infinito deve essere fatto sia sui
 *            vicini che sul device stesso.  
 * */ 
 
module C_emp 
 
import java.lang.Double.* 
import java.lang.Math.* 
import utils 
 
public def d1 (radius, p, b) { gauss(self.nbrRange()/radius, 1, p, b) }
 
public def d2 (radius, p, b) { atanD(self.nbrRange()/radius, b*100, p) }

public def d3 (radius, p, b) { max(radius - self.nbrRange(), 0) }
 
def p (potential) { 
 	
 	mux (isInfinite(potential) || nbr(isInfinite(potential))) { 0 } 
 	else { abs(nbr(potential) - potential) } 
} 
 
def goodness (potential, radius, d, p, b) { d.apply(radius, p, b) * p(potential) } 
 
def nbrSlice (potential, goodness) {
 	
 	let higherP = nbr(potential) > potential;
 	let N = nbr(sumHood(mux (higherP) { 0 } else { goodness })); 
    
    mux (higherP && N != 0) { goodness / N } else { 0 }
} 

public def C_EMP (potential, accumulate, local, null, root, radius, d, p, b) { 
	
	rep (x <- local) {
        
        let goodness = goodness(potential, radius, d, p, b); 
        let nbrSlice = nbrSlice(potential, goodness); 
        let nbrValue = root.apply(nbr(x), nbrSlice); 
        
        accumulate.apply(local, foldHood(accumulate, nbrValue, null)); 
    } 
}